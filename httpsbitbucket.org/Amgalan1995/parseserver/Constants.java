package ru.vsgtu.schedule.server;

/**
 * Created by HOME on 24.04.2016.
 */
public class Constants {
    // Spring profile for development and production, see http://jhipster.github.io/profiles/
    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_PRODUCTION = "prod";
    public static final String SPRING_PROFILE_CLOUD = "cloud";
    public static final String SPRING_PROFILE_NO_LIQUIBASE = "no-liquibase";
}
