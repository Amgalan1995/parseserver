package ru.vsgtu.schedule.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vsgtu.schedule.server.domain.Auditory;

import java.util.Optional;

/**
 * Created by HOME on 24.04.2016.
 */
public interface AuditoryRepository extends JpaRepository<Auditory, Long> {
    Optional<Auditory> findOneByName(String name);

    Optional<Auditory> findOneById(Long id);
}
