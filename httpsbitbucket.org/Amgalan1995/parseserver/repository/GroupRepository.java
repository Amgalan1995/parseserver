package ru.vsgtu.schedule.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vsgtu.schedule.server.domain.Group;

import java.util.Optional;

/**
 * Created by HOME on 24.04.2016.
 */
public interface GroupRepository extends JpaRepository<Group, Long> {
    Optional<Group> findOneByName(String name);
}
