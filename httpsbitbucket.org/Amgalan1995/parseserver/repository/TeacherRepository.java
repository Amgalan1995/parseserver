package ru.vsgtu.schedule.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vsgtu.schedule.server.domain.Teacher;

import java.util.Optional;

/**
 * Created by HOME on 01.05.2016.
 */
public interface TeacherRepository extends JpaRepository<Teacher, Long> {

    Optional<Teacher> findOneById(Long id);
}
