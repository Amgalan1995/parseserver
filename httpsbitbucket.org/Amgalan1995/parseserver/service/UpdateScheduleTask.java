package ru.vsgtu.schedule.server.service;

import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.vsgtu.schedule.server.OptionalConsumer;
import ru.vsgtu.schedule.server.config.AppConfigurationProperties;
import ru.vsgtu.schedule.server.domain.Auditory;
import ru.vsgtu.schedule.server.domain.Main;
import ru.vsgtu.schedule.server.domain.MainStatus;
import ru.vsgtu.schedule.server.domain.Subject;
import ru.vsgtu.schedule.server.domain.SubjectType;
import ru.vsgtu.schedule.server.domain.Teacher;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by HOME on 24.04.2016.
 */

@Component
@Singleton
public class UpdateScheduleTask {
    private final Logger log = LoggerFactory.getLogger(UpdateScheduleTask.class);
    private static final String QUERY_GET_GROUP = "font[size=6]";
    private static final String QUERY_GET_ROWS = "body > table > tbody";
    private static final String DOCUMENT_ENCODING = "windows-1251";

    private static final String FIELD_ID = "id";
    private static final String FIELD_BODY = "body";

    private Map<String, Item> store = new HashMap<>();


    private Map<Pattern, String> synonium;

    private IndexSearcher indexSearcher;

    private final ReentrantLock lock = new ReentrantLock();

    @Inject
    private IndexWriter indexWriter;

    @Inject
    private AppConfigurationProperties appConfigurationProperties;

    @Inject
    private ScheduleService scheduleService;

    @Inject
    private ExecutionLogService executionLogService;

    //@Scheduled(cron = "${app.scheduleTask.cron}")
    public void update() {
        if (lock.tryLock()) {
            LocalDateTime start = LocalDateTime.now();

            log.info("Start Task");
            if (appConfigurationProperties.getSchedule().getUrl() == null) {
                log.error("Need set url in application.yml");
                return;
            }
            try {
                for (String url : appConfigurationProperties.getSchedule().getUrl()) {
                    log.debug("Process url {}", url);
                    Document tree = Jsoup.connect(url).get();
                    doParseTreeAddToLucene(tree);
                }
                doSearchTeacher();
                doSearchSubject();
                doSearchTypeSubject();
                doSearchAuditory();
                doLog();
                doUpdateDMain();
                executionLogService.success(start);
            } catch (Exception e) {
                executionLogService.error(start, e);
                log.error("Error {}", appConfigurationProperties.getSchedule().getUrl(), e);
            } finally {
                lock.unlock();
                try {
                    indexWriter.deleteAll();
                } catch (IOException e) {
                    log.error("Error", e);
                }
            }
            log.info("End Task");
        }
    }

    void doUpdateDMain() throws IOException {
        List<Main> list = store.values().stream().map(item -> {
            Main main = new Main();
            main.setMessage(item.message);
            main.setStatus(MainStatus.SUCCESS);
            main.setWeekDay(item.week);
            main.setSubjectType(item.typeSubject);
            main.setLessonNumber(item.numberLesson);
            Optional.ofNullable(item.subjectId).map(id -> scheduleService.getSubjectById(id)).ifPresent(main::setSubject);
            Optional.ofNullable(item.auditoryId).map(id -> scheduleService.getAuditoryById(id)).ifPresent(main::setAuditory);
            Optional.ofNullable(item.teacherId).map(id -> scheduleService.getTeacherById(id)).ifPresent(main::setTeacher);
            Optional.ofNullable(item.groupId).map(id -> scheduleService.getGroupById(id)).ifPresent(main::setGroup);
            Stream.builder().
                    add(main.getAuditory()).
                    add(main.getSubject()).
                    add(main.getGroup()).
                    add(main.getTeacher()).
                    build().map(Optional::ofNullable).
                    filter(e -> !e.isPresent()).
                    findAny().
                    ifPresent(e -> {
                        main.setStatus(MainStatus.PARTIAL);
                    });
            return main;
        }).collect(Collectors.toList());
        log.debug("Update main size {}", list.size());
        scheduleService.updateMainUpdate(list);
        store.clear();
    }

    void doLog() throws IOException {
        store.forEach((key, value) -> {
            if (log.isTraceEnabled()) {
                String subject = value.subjectId == null ? "Not found" : scheduleService.getSubjectById(value.subjectId).getName();
                String teacher = value.teacherId == null ? "Not found" : scheduleService.getTeacherById(value.teacherId).getName();
                String auditory = value.auditoryId == null ? "Not found" : scheduleService.getAuditoryById(value.auditoryId).getName();
                log.trace("Doc {} \n body {} \n subject {} \n teacher {} \n autidory {}", key, value.message, subject, teacher, auditory);
            }
        });
    }

    void doSearchTeacher() throws ParseException, IOException {
        QueryParser parser = new QueryParser(FIELD_BODY, new RussianAnalyzer());
        for (Teacher teacher : scheduleService.getTeacherAll()) {
            Query query = parser.parse("\"" + teacher.getName() + "\"");
            TopDocs hits = indexSearcher.search(query, Integer.MAX_VALUE);
            for (ScoreDoc scoreDoc : hits.scoreDocs) {
                org.apache.lucene.document.Document doc = indexSearcher.doc(scoreDoc.doc);
                log.trace("Found teacher {} \n  for content {}", teacher.getName(), doc.get(FIELD_BODY));
                String docId = doc.get(FIELD_ID);
                Item item = store.get(docId);
                item.teacherId = teacher.getId();
            }
        }
    }

    void doSearchAuditory() throws ParseException, IOException {
        Map<String, Float> score = new HashMap<>();
        QueryParser parser = new QueryParser(FIELD_BODY, new RussianAnalyzer());
        for (Auditory auditory : scheduleService.getAuditoryAll()) {
            Query query = parser.parse("\"" + auditory.getName() + "\"");
            TopDocs hits = indexSearcher.search(query, Integer.MAX_VALUE);
            for (ScoreDoc scoreDoc : hits.scoreDocs) {
                org.apache.lucene.document.Document doc = indexSearcher.doc(scoreDoc.doc);
                String docId = doc.get(FIELD_ID);
                Item item = store.get(docId);
                log.trace("Found auditory {}  for content {}", auditory.getName(), item.message);
                if (score.get(docId) == null) {
                    score.put(docId, scoreDoc.score);
                    item.auditoryId = auditory.getId();
                } else if (scoreDoc.score > score.get(docId)) {
                    log.trace("Update auditory get more score {} vs {} ", score.get(docId), scoreDoc.score);
                    item.auditoryId = auditory.getId();
                }
            }
        }
    }

    void doSearchTypeSubject() throws IOException, ParseException {
        Map<String, Float> score = new HashMap<>();
        QueryParser parser = new QueryParser(FIELD_BODY, new RussianAnalyzer());
        for (SubjectType type : SubjectType.values()) {
            Query query = parser.parse(type.getTitle() + "~3");
            TopDocs hits = indexSearcher.search(query, Integer.MAX_VALUE);
            for (ScoreDoc scoreDoc : hits.scoreDocs) {
                org.apache.lucene.document.Document doc = indexSearcher.doc(scoreDoc.doc);
                String docId = doc.get(FIELD_ID);
                Item item = store.get(docId);
                item.typeSubject = type;
                log.trace("Found subject {} for content {}", type.getName(), item.message);
            }
        }
    }

    void doSearchSubject() throws ParseException, IOException {
        Map<String, Float> score = new HashMap<>();
        QueryParser parser = new QueryParser(FIELD_BODY, new RussianAnalyzer());
        for (Subject subject : scheduleService.getSubjectAll()) {
            Query query = parser.parse("\"" + subject.getShortName() + "\"~4");
            TopDocs hits = indexSearcher.search(query, Integer.MAX_VALUE);
            for (ScoreDoc scoreDoc : hits.scoreDocs) {
                org.apache.lucene.document.Document doc = indexSearcher.doc(scoreDoc.doc);
                String docId = doc.get(FIELD_ID);
                Item item = store.get(docId);
                log.trace("Found subject {} for content {}", subject.getName(), item.message);
                if (score.get(docId) == null) {
                    score.put(docId, scoreDoc.score);
                    item.subjectId = subject.getId();
                } else if (scoreDoc.score > score.get(docId)) {
                    log.trace("Update subject get more score {} vs {} ", score.get(docId), scoreDoc.score);
                    item.subjectId = subject.getId();
                }
            }
        }
    }


    void doParseTreeAddToLucene(Document tree) throws IOException {
        Element table = tree.getElementsByTag("tbody").first();
        Elements anchors = table.getElementsByTag("a");
        anchors.stream().
                filter(e -> !e.child(0).text().isEmpty()).
                map(e -> e.getElementsByAttribute("href")).
                forEach(anchor -> {
                    Optional<String> o = Optional.ofNullable(anchor.attr("abs:href")).
                            filter(h -> !h.isEmpty()).
                            filter(h -> true /* check valid url */);
                    OptionalConsumer.of(o).ifPresent(href -> {
                        processHref(URI.create(href));
                    }).ifNotPresent(() -> {
                        log.error("Invalid url: /{}/", anchor.attr("href"));
                    });
                });
        indexWriter.commit();
        indexSearcher = new IndexSearcher(DirectoryReader.open(indexWriter));
    }

    void processHref(URI uri) {
        log.trace("Process href {}", uri.toASCIIString());
        try {
            doGroupSchedule(Jsoup.parse(uri.toURL().openStream(), DOCUMENT_ENCODING, ""));
        } catch (IOException e) {
            log.error("Error for href= {}", uri.toASCIIString(), e);
        }
    }

    void doGroupSchedule(Document tree) {
        String groupName = tree.select(QUERY_GET_GROUP).text();
        log.trace("Process group {}", groupName);
        AtomicInteger index = new AtomicInteger(1);
        Element table = tree.select(QUERY_GET_ROWS).first();
        table.children().stream().
                skip(2).
                forEach(e -> {
                    processTR(e, index.getAndIncrement(), scheduleService.getByName(groupName).getId());
                });
    }

    void processTR(Element tr, int index, Long groupId) {
        Stream.of(tr).
                flatMap(e -> e.children().stream().skip(1).map(td -> td.select("p > font").text())).
                filter(this::filter).
                map(this::prepearAcronium).
                forEach(body -> {
                    try {
                        log.trace("Store content {}", body);
                        org.apache.lucene.document.Document document = new org.apache.lucene.document.Document();
                        String docId = java.util.UUID.randomUUID().toString();
                        document.add(new StoredField(FIELD_ID, docId));
                        document.add(new TextField(FIELD_BODY, body, Field.Store.YES));
                        store.put(docId, new Item(groupId, (index > 6 ? 12 - index : index), (index > 6 ? 2 : 1), body));
                        indexWriter.addDocument(document);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    boolean filter(String s) {
        return !s.equals("_") && !s.isEmpty();
    }

    String prepearAcronium(String word) {
        AtomicReference<String> stringRef = new AtomicReference<>(word);
        OptionalConsumer.of(Optional.ofNullable(word)).ifPresent(s -> {
            for (Map.Entry<Pattern, String> entry : synonium.entrySet()) {
                stringRef.set(entry.getKey().matcher(stringRef.get()).replaceAll(entry.getValue()));
            }
        }).ifNotPresent(() -> log.error(" Invalid word {}", word));
        return stringRef.get();
    }

    @PostConstruct
    void init() {
        synonium = new HashMap<>();
        synonium.put(Pattern.compile("пр\\."), "практика ");
        synonium.put(Pattern.compile("лек\\."), "лекция ");
        //synonium.put(Pattern.compile("а\\."), "аудитория ");
        synonium.put(Pattern.compile("лаб\\."), "лабороторная ");
        synonium.put(Pattern.compile("и/д"), " ");
    }

    private static class Item {
        private Long subjectId;
        private Long groupId;
        private Long teacherId;
        private Long auditoryId;
        private SubjectType typeSubject;
        private String message;
        private int numberLesson;
        private int week;

        public Item(Long groupId, int numberLesson, int week, String message) {
            this.groupId = groupId;
            this.numberLesson = numberLesson;
            this.message = message;
            this.week = week;
        }


    }
}
