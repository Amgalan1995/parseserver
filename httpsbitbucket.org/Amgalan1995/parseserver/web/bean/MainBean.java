package ru.vsgtu.schedule.server.web.bean;

import org.primefaces.context.RequestContext;
import ru.vsgtu.schedule.server.domain.Main;
import ru.vsgtu.schedule.server.service.ScheduleService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Named("mains")
@SessionScoped
public class MainBean {
    @Inject
    private ScheduleService scheduleService;
    private List<Main> list = Collections.emptyList();
    private Main edit;

    @PostConstruct
    public void init() {
        list = scheduleService.getMainAll();
    }

    public List<Main> getList() {
        return list;
    }

    public void loadEntity(Long id) {
        edit = scheduleService.getMain(id);
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("resizable", false);
        options.put("draggable", false);
        RequestContext.getCurrentInstance().openDialog("page/edit_main.xhtml", options, null);
    }

    public Main getEdit() {
        return edit;
    }

    public void cancel() {
        RequestContext.getCurrentInstance().closeDialog(null);
    }
}
