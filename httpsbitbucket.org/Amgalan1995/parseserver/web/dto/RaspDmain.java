
package ru.vsgtu.schedule.server.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "grup",
    "dz",
    "para",
    "dis",
    "vid",
    "aud",
    "prep",
    "tip",
    "fak",
    "kor"
})
public class RaspDmain {

    @JsonProperty("grup")
    private String grup;
    @JsonProperty("dz")
    private String dz;
    @JsonProperty("para")
    private String para;
    @JsonProperty("dis")
    private String dis;
    @JsonProperty("vid")
    private String vid;
    @JsonProperty("aud")
    private String aud;
    @JsonProperty("prep")
    private String prep;
    @JsonProperty("tip")
    private String tip;
    @JsonProperty("fak")
    private String fak;
    @JsonProperty("kor")
    private String kor;

    /**
     * 
     * @return
     *     The grup
     */
    @JsonProperty("grup")
    public String getGrup() {
        return grup;
    }

    /**
     * 
     * @param grup
     *     The grup
     */
    @JsonProperty("grup")
    public void setGrup(String grup) {
        this.grup = grup;
    }

    /**
     * 
     * @return
     *     The dz
     */
    @JsonProperty("dz")
    public String getDz() {
        return dz;
    }

    /**
     * 
     * @param dz
     *     The dz
     */
    @JsonProperty("dz")
    public void setDz(String dz) {
        this.dz = dz;
    }

    /**
     * 
     * @return
     *     The para
     */
    @JsonProperty("para")
    public String getPara() {
        return para;
    }

    /**
     * 
     * @param para
     *     The para
     */
    @JsonProperty("para")
    public void setPara(String para) {
        this.para = para;
    }

    /**
     * 
     * @return
     *     The dis
     */
    @JsonProperty("dis")
    public String getDis() {
        return dis;
    }

    /**
     * 
     * @param dis
     *     The dis
     */
    @JsonProperty("dis")
    public void setDis(String dis) {
        this.dis = dis;
    }

    /**
     * 
     * @return
     *     The vid
     */
    @JsonProperty("vid")
    public String getVid() {
        return vid;
    }

    /**
     * 
     * @param vid
     *     The vid
     */
    @JsonProperty("vid")
    public void setVid(String vid) {
        this.vid = vid;
    }

    /**
     * 
     * @return
     *     The aud
     */
    @JsonProperty("aud")
    public String getAud() {
        return aud;
    }

    /**
     * 
     * @param aud
     *     The aud
     */
    @JsonProperty("aud")
    public void setAud(String aud) {
        this.aud = aud;
    }

    /**
     * 
     * @return
     *     The prep
     */
    @JsonProperty("prep")
    public String getPrep() {
        return prep;
    }

    /**
     * 
     * @param prep
     *     The prep
     */
    @JsonProperty("prep")
    public void setPrep(String prep) {
        this.prep = prep;
    }

    /**
     * 
     * @return
     *     The tip
     */
    @JsonProperty("tip")
    public String getTip() {
        return tip;
    }

    /**
     * 
     * @param tip
     *     The tip
     */
    @JsonProperty("tip")
    public void setTip(String tip) {
        this.tip = tip;
    }

    /**
     * 
     * @return
     *     The fak
     */
    @JsonProperty("fak")
    public String getFak() {
        return fak;
    }

    /**
     * 
     * @param fak
     *     The fak
     */
    @JsonProperty("fak")
    public void setFak(String fak) {
        this.fak = fak;
    }

    /**
     * 
     * @return
     *     The kor
     */
    @JsonProperty("kor")
    public String getKor() {
        return kor;
    }

    /**
     * 
     * @param kor
     *     The kor
     */
    @JsonProperty("kor")
    public void setKor(String kor) {
        this.kor = kor;
    }
}
