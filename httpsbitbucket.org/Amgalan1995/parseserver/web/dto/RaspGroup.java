
package ru.vsgtu.schedule.server.web.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "grup",
    "tip"
})
public class RaspGroup {

    @JsonProperty("grup")
    private String grup;
    @JsonProperty("tip")
    private String tip;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The grup
     */
    @JsonProperty("grup")
    public String getGrup() {
        return grup;
    }

    /**
     * 
     * @param grup
     *     The grup
     */
    @JsonProperty("grup")
    public void setGrup(String grup) {
        this.grup = grup;
    }

    /**
     * 
     * @return
     *     The tip
     */
    @JsonProperty("tip")
    public String getTip() {
        return tip;
    }

    /**
     * 
     * @param tip
     *     The tip
     */
    @JsonProperty("tip")
    public void setTip(String tip) {
        this.tip = tip;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
