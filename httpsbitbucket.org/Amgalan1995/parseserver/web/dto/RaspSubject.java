
package ru.vsgtu.schedule.server.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "dis",
    "diss",
    "disp"
})
public class RaspSubject {

    @JsonProperty("dis")
    private String dis;
    @JsonProperty("diss")
    private String diss;
    @JsonProperty("disp")
    private String disp;

    /**
     * 
     * @return
     *     The dis
     */
    @JsonProperty("dis")
    public String getDis() {
        return dis;
    }

    /**
     * 
     * @param dis
     *     The dis
     */
    @JsonProperty("dis")
    public void setDis(String dis) {
        this.dis = dis;
    }

    /**
     * 
     * @return
     *     The diss
     */
    @JsonProperty("diss")
    public String getDiss() {
        return diss;
    }

    /**
     * 
     * @param diss
     *     The diss
     */
    @JsonProperty("diss")
    public void setDiss(String diss) {
        this.diss = diss;
    }

    /**
     * 
     * @return
     *     The disp
     */
    @JsonProperty("disp")
    public String getDisp() {
        return disp;
    }

    /**
     * 
     * @param disp
     *     The disp
     */
    @JsonProperty("disp")
    public void setDisp(String disp) {
        this.disp = disp;
    }
}
