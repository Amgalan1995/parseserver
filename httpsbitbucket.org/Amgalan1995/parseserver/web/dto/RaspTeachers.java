
package ru.vsgtu.schedule.server.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "prep",
    "fio",
    "dol",
    "kafs",
    "gr",
    "pr"
})
public class RaspTeachers {

    @JsonProperty("prep")
    private String prep;
    @JsonProperty("fio")
    private String fio;
    @JsonProperty("dol")
    private String dol;
    @JsonProperty("kafs")
    private String kafs;
    @JsonProperty("gr")
    private String gr;
    @JsonProperty("pr")
    private String pr;

    /**
     * 
     * @return
     *     The prep
     */
    @JsonProperty("prep")
    public String getPrep() {
        return prep;
    }

    /**
     * 
     * @param prep
     *     The prep
     */
    @JsonProperty("prep")
    public void setPrep(String prep) {
        this.prep = prep;
    }

    /**
     * 
     * @return
     *     The fio
     */
    @JsonProperty("fio")
    public String getFio() {
        return fio;
    }

    /**
     * 
     * @param fio
     *     The fio
     */
    @JsonProperty("fio")
    public void setFio(String fio) {
        this.fio = fio;
    }

    /**
     * 
     * @return
     *     The dol
     */
    @JsonProperty("dol")
    public String getDol() {
        return dol;
    }

    /**
     * 
     * @param dol
     *     The dol
     */
    @JsonProperty("dol")
    public void setDol(String dol) {
        this.dol = dol;
    }

    /**
     * 
     * @return
     *     The kafs
     */
    @JsonProperty("kafs")
    public String getKafs() {
        return kafs;
    }

    /**
     * 
     * @param kafs
     *     The kafs
     */
    @JsonProperty("kafs")
    public void setKafs(String kafs) {
        this.kafs = kafs;
    }

    /**
     * 
     * @return
     *     The gr
     */
    @JsonProperty("gr")
    public String getGr() {
        return gr;
    }

    /**
     * 
     * @param gr
     *     The gr
     */
    @JsonProperty("gr")
    public void setGr(String gr) {
        this.gr = gr;
    }

    /**
     * 
     * @return
     *     The pr
     */
    @JsonProperty("pr")
    public String getPr() {
        return pr;
    }

    /**
     * 
     * @param pr
     *     The pr
     */
    @JsonProperty("pr")
    public void setPr(String pr) {
        this.pr = pr;
    }
}
